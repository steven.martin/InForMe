#pragma once

#include <QWidget>
#include "ui_cameraview.h"

class CameraView : public QWidget
{
	Q_OBJECT

public:
	CameraView(QWidget *parent = nullptr);
	~CameraView();

private:
	Ui::cameraViewClass ui;
};
