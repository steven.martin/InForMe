#pragma once

#include <QWidget>
#include "ui_holderview.h"

class HolderView : public QWidget
{
	Q_OBJECT

public:
	HolderView(QWidget *parent = nullptr);
	~HolderView();

private:
	Ui::holderViewClass ui;
};
