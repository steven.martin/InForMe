#include "lowerlayout.h"
#include "forceview.h"
#include "informe.h"

LowerLayout::LowerLayout(int pagenumber, QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	ui.controlWinButton->setVisible(false);
	if (pagenumber == 1) {
		ui.backButton->setVisible(false);
	
	}

	if (pagenumber == 2) {
		ui.nextButton->setVisible(false);
	}
	connect(ui.nextButton, &QPushButton::clicked, this, &LowerLayout::nextButtonPressed);
	connect(ui.backButton, &QPushButton::clicked, this, &LowerLayout::onBackButtonClicked);

}

LowerLayout::~LowerLayout()
{}

void LowerLayout::onBackButtonClicked() {

	emit backButtonPressed();
}

void LowerLayout::forceSensorVerify() {

	if (!ui.nextButton->isVisible()) {
		ui.nextButton->isVisible();
	}
	ui.nextButton->setVisible(true);
}