#pragma once

#include <QObject>

class ThreadStorage  : public QObject
{
	Q_OBJECT

public:
	ThreadStorage(QObject *parent);
	~ThreadStorage();
};
