#include "imuview.h"

IMUView::IMUView(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

    connect(ui.connectButton_3, &QPushButton::clicked, this, &IMUView::checkIfIMUConnected);

    connect(ui.statusGyro_3, &QPushButton::clicked, this, &IMUView::checkIfGyroCalibrated);
    connect(ui.statusMagneto_3, &QPushButton::clicked, this, &IMUView::checkIfMagnetoCalibrated);
    connect(ui.statusAccel_3, &QPushButton::clicked, this, &IMUView::checkIfAccelCalibrated);
    connect(this, &IMUView::gyroCalibrated, this, &IMUView::onIfGyroCalibrated);
    connect(this, &IMUView::magnetoCalibrated, this, &IMUView::onIfMagnetoCalibrated);
    connect(this, &IMUView::accelCalibrated, this, &IMUView::onIfAccelCalibrated);
    ui.gyroLabel_3->setVisible(false);
    ui.statusGyro_3->setVisible(false);
    ui.magnetoLabel_3->setVisible(false);
    ui.statusMagneto_3->setVisible(false);
    ui.accelLabel_3->setVisible(false);
    ui.statusAccel_3->setVisible(false);

}

IMUView::~IMUView()
{}


void IMUView::checkIfIMUConnected() {


    if (this->statusIMUConnected == false) {
        ui.connectButton_3->setText("Connection Failed!");
        ui.descLabel_3->setText("-Please check USB-plus!");
        this->statusIMUConnected = true;
    }
    else if (this->statusIMUConnected) {
        ui.connectButton_3->setText("Connected!");
        ui.connectButton_3->setStyleSheet("background-color: #ADDE87; border-style: outset; border - width: 2px; border-radius: 10px; border-color: beige; font: 14px; padding: 6px; ");
        ui.descLabel_3->setText("Connection Successful! Please Calibrate IMU!");
        ui.gyroLabel_3->setVisible(true);
        ui.statusGyro_3->setVisible(true);
    }
}

void IMUView::checkIfGyroCalibrated() {

    this->statusGyroCalibrated = true;
    emit gyroCalibrated();
}
void IMUView::onIfGyroCalibrated() {
    ui.gyroLabel_3->setText("Gyro Calibrated!");
    ui.label_17->setStyleSheet("image: url(:/InForMe/Frame 14.jpg);");
    ui.magnetoLabel_3->setVisible(true);
    ui.statusMagneto_3->setVisible(true);
}

void IMUView::checkIfMagnetoCalibrated() {

    this->statusMagnetoCalibrated = true;
    emit magnetoCalibrated();
}
void IMUView::onIfMagnetoCalibrated() {
    ui.magnetoLabel_3->setText("Magneto Calibrated!");
    ui.label_17->setStyleSheet("image: url(:/InForMe/Frame 15.jpg);");
    ui.accelLabel_3->setVisible(true);
    ui.statusAccel_3->setVisible(true);
}
void IMUView::checkIfAccelCalibrated() {

    this->statusAccelCalibrated = true;
    emit accelCalibrated();
}
void IMUView::onIfAccelCalibrated() {
    ui.accelLabel_3->setText("Accel Calibrated!");
}