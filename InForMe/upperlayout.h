#pragma once

#include <QWidget>
#include "ui_upperlayout.h"

class UpperLayout : public QWidget
{
	Q_OBJECT

public:
	UpperLayout(QWidget *parent = nullptr);
	~UpperLayout();

private:
	Ui::upperLayoutClass ui;
};
