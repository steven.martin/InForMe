#pragma once

#include <QWidget>
#include "ui_lowerlayout.h"

class LowerLayout : public QWidget
{
	Q_OBJECT

public:
	LowerLayout(int pagenumber, QWidget *parent = nullptr);
	~LowerLayout();
	void forceSensorVerify();

signals:
	void nextButtonPressed();
	void backButtonPressed();


private:
	Ui::lowerLayoutClass ui;

	void onBackButtonClicked();

};
