#pragma once

#include <QWidget>
#include "ui_imuview.h"

class IMUView : public QWidget
{
	Q_OBJECT

public:
	IMUView(QWidget *parent = nullptr);
	~IMUView();

signals:

	void gyroCalibrated();
	void magnetoCalibrated();
	void accelCalibrated();



private:
	Ui::IMUViewClass ui;
   void checkIfIMUConnected();
   void checkIfGyroCalibrated();
   void checkIfMagnetoCalibrated();
   void checkIfAccelCalibrated();
   void onIfGyroCalibrated();
   void onIfMagnetoCalibrated();
   void onIfAccelCalibrated();
   //#Page3
   bool statusIMUConnected = false;
   bool statusGyroCalibrated = false;
   bool statusMagnetoCalibrated = false;
   bool statusAccelCalibrated = false;

};
