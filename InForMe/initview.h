#pragma once

#include <QWidget>
#include "ui_initview.h"

class InitView : public QWidget
{
	Q_OBJECT

public:
	InitView(QWidget *parent = nullptr);
	~InitView();

private:
	Ui::initViewClass ui;
};
