#pragma once

#include <QWidget>
#include "ui_forceview.h"

class ForceView : public QWidget
{
	Q_OBJECT

public:
	ForceView(QWidget *parent = nullptr);
	~ForceView();

signals:
	void forceSensorVerified(int pagenum);

private:
	Ui::forceViewClass ui;
	QString connectedStyle;
	//#Page2
	bool statusForceSensorConnected = false;
	bool statusForceCensorVerified = false;

	void checkIfForceConnected();
	void checkIfForceVerified();
};
