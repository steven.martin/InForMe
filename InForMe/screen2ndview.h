#pragma once

#include <QWidget>
#include "ui_screen2ndview.h"

class Screen2ndView : public QWidget
{
	Q_OBJECT

public:
	Screen2ndView(QWidget *parent = nullptr);
	~Screen2ndView();

	void onNoButtonClicked();
	void onYesButtonClicked();

private:
	Ui::screen2ndViewClass ui;
};
