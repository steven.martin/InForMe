#include "forceview.h"

ForceView::ForceView(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	connect(ui.connectButton_2, &QPushButton::clicked, this, &ForceView::checkIfForceConnected);
	connect(ui.verifyButton_2, &QPushButton::clicked, this, &ForceView::checkIfForceVerified);
    this->connectedStyle = "QPushButton {background-color: #ADDE87; border-style: outset; border-width: 2px; border-radius: 10px; border-color: beige; font: bold 14px; padding: 6px;}";
    ui.verifyButton_2->setVisible(false);
}

ForceView::~ForceView()
{}


void ForceView::checkIfForceConnected() {


    if (this->statusForceSensorConnected == false) {
        ui.connectButton_2->setText("Connection Failed!");
        ui.descLabel_2->setText("-Please check USB-plus! \n-Please enable amplifier (GSV - 8)");
        this->statusForceSensorConnected = true;
    }
    else if (this->statusForceSensorConnected) {
        ui.connectButton_2->setText("Connected!");
        ui.connectButton_2->setStyleSheet(this->connectedStyle);
        ui.descLabel_2->setText("Connection Successful! Please Verify Sensor!");
        ui.verifyButton_2->setVisible(true);
    }
}

void ForceView::checkIfForceVerified() {

    this->statusForceCensorVerified = true;
    ui.verifyButton_2->setText("Verify Successful!");
    ui.verifyButton_2->setStyleSheet(this->connectedStyle);
    emit forceSensorVerified(2);
}
