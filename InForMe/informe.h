#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_informe.h"
#include "cameraview.h"
#include "controlview.h"
#include "forceview.h"
#include "holderview.h"
#include "imuview.h"
#include "initview.h"
#include "lowerlayout.h"
#include "screen2ndview.h"
#include "upperlayout.h"

class InForMe : public QMainWindow
{
    Q_OBJECT

public:
    InForMe(QWidget *parent = nullptr);
    ~InForMe();

signals:
    int currentPageNumberSignal(int pagenum);

private:

    //Int
    int currentPage;


    HolderView* HolView;
    
    Screen2ndView* SecondScreenView;
    ControlView* ConView;
    
    UpperLayout* UpLayout;

    ForceView* ForceSensor;
    IMUView* IMU;
    InitView* Init;

    LowerLayout* LowLayout;


    //Bool (Status connected, completed, etc.)

    //#Page4
    bool statusElectroHolderChosen;

    //#Page5
    bool statusVideoConnected;

    //#Page6
    bool statusSecondMonitor;


    //Buttons pressed

    void onNextButtonClicked();
    void onBackButtonClicked();
    void animationStackedWidgets();
    void whenAnimationFinish();

    void updateProgressBar(int pagenum);
    void updatePageNumber(int pagenum);



    Ui::InForMeClass ui;


};
