#pragma once

#include <QWidget>
#include "ui_controlview.h"

class ControlView : public QWidget
{
	Q_OBJECT

public:
	ControlView(QWidget *parent = nullptr);
	~ControlView();

private:
	Ui::controlViewClass ui;
};
