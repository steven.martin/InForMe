#include "informe.h"
#include <QGraphicsOpacityEffect>
#include <QPropertyAnimation>
#include <QDeadlineTimer>
#include "qfile.h"
#include "qfileinfo.h"
#include "qdebug.h"
#include "qprogressbar.h"

InForMe::InForMe(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    //Initialize Widgets
   

    this->Init = new InitView(this);


    this->ForceSensor = new ForceView(this);

    this->HolView = new HolderView(this);
    this->UpLayout = new UpperLayout(this);

    this->LowLayout = new LowerLayout(1, this);


    this->SecondScreenView = new Screen2ndView(this);

    this->ConView = new ControlView(this);

    this->IMU = new IMUView(this);

    ui.lowerWidgetLayout->addWidget(this->LowLayout);
    ui.upperWidgetLayout->addWidget(this->UpLayout);

    //page 1
    ui.InitMiddleLayout->addWidget(this->Init);

    //page 2
    ui.ForceMiddleLayout->addWidget(this->ForceSensor);

    //page 3
    ui.IMUMiddleLayout->addWidget(this->IMU);

    //page4
    ui.ElectroHolderMiddleLayout->addWidget(this->HolView);

    //page 5

    //page6
    ui.SecondScreenMiddleLayour->addWidget(this->SecondScreenView);

    //page 7
    ui.controlWindowMiddleLayout->addWidget(this->ConView);

    connect(this->LowLayout, &LowerLayout::nextButtonPressed, this, &InForMe::onNextButtonClicked);
    connect(this->LowLayout, &LowerLayout::backButtonPressed, this, &InForMe::onBackButtonClicked);
    connect(this->ForceSensor, &ForceView::forceSensorVerified, this, &InForMe::updateProgressBar);

    connect(this, &InForMe::currentPageNumberSignal, this, &InForMe::updatePageNumber);
    ui.widget_4->setVisible(false);
    ui.connectionProgressBar->setMinimum(1);
    ui.connectionProgressBar->setMaximum(6);
    ui.connectionProgressBar->setVisible(false);

    ui.stackedWidget->setCurrentIndex(0);

    QString fileName("testing.csv");
    QFile file(fileName);
    if (QFileInfo::exists(fileName))
    {
        qDebug() << "file exists" ;
        file.open(QIODevice::ReadWrite | QIODevice::Text);
        QString data = file.readAll();
        qDebug() << "data in file:" << data;
        qDebug() << "file already created";
        file.close();
        ui.widget_4->setVisible(true);
    }
    else
    {
        qDebug() << "file does not exists";
        // file.open(QIODevice::ReadWrite | QIODevice::Text);
        // file.write("testing");
         //qDebug() << "file created";
        // file.close();
    }
    
    this->currentPage = 0;

}

InForMe::~InForMe()
{}

void InForMe::onNextButtonClicked(){
    
    if (this->currentPage < 7) {
        this->currentPage = this->currentPage + 1;
        ui.stackedWidget->setCurrentIndex(this->currentPage);
        emit currentPageNumberSignal(this->currentPage);
    }
}

void InForMe::onBackButtonClicked() {

    if (this->currentPage > 0) {
        this->currentPage = this->currentPage - 1;
        ui.stackedWidget->setCurrentIndex(this->currentPage);
        emit currentPageNumberSignal(this->currentPage);
    }
}

void InForMe::updatePageNumber(int pagenum) {

    if (pagenum == 1) {
        ui.connectionProgressBar->setVisible(true);
        
    }

}

void InForMe::updateProgressBar(int pagenum){

     ui.connectionProgressBar->setValue(pagenum);

}














//animation Widget, to optimize if to be implemented
void InForMe::animationStackedWidgets()
    {
    QGraphicsOpacityEffect* effect = new QGraphicsOpacityEffect(this);
    ui.stackedWidget->setGraphicsEffect(effect);
    QPropertyAnimation* anim = new QPropertyAnimation(effect, "opacity");
    anim->setDuration(350);
    anim->setStartValue(0);
    anim->setEndValue(1);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->start(QPropertyAnimation::DeleteWhenStopped);


        connect(anim, SIGNAL(finished()), this, SLOT(whenAnimationFinish()));
        ui.stackedWidget->setCurrentIndex(1);
        this->currentPage = 1;
    }

void InForMe::whenAnimationFinish()
    {
        ui.stackedWidget->setGraphicsEffect(0); // remove effect
        
    }
