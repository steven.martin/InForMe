#include "screen2ndview.h"
#include "qscreen.h"

Screen2ndView::Screen2ndView(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	ui.secondscreenDock->setVisible(false);

	connect(ui.yesButton, &QPushButton::clicked, this, &Screen2ndView::onYesButtonClicked);
	connect(ui.noButton, &QPushButton::clicked, this, &Screen2ndView::onNoButtonClicked);
	
	qDebug() << "Screen Count:" << QGuiApplication::screens().size();
	if (QGuiApplication::screens().size() < 2) {
		ui.yesButton->setEnabled(false);
	}

}

Screen2ndView::~Screen2ndView()
{
}


void Screen2ndView::onYesButtonClicked(){

	if (!ui.secondscreenDock->isVisible()) {
		ui.secondscreenDock->setVisible(true);
		
	}
	ui.secondscreenDock->setFloating(true);
	QScreen* screen = QGuiApplication::screens()[1]; // specify which screen to use

	
	//SecondDisplay secondDisplay = new SecondDisplay(); // your widget

	ui.secondscreenDock->move(screen->geometry().x(), screen->geometry().y());
	ui.secondscreenDock->resize(screen->geometry().width(), screen->geometry().height());
	ui.secondscreenDock->showFullScreen();
}

void Screen2ndView::onNoButtonClicked() {

	if (!ui.secondscreenDock->isVisible()) {
		ui.secondscreenDock->setVisible(true);
		ui.secondscreenDock->setFloating(true);	
	}

	ui.secondscreenDock->setFeatures(QDockWidget::DockWidgetMovable);
	QScreen* screen = QGuiApplication::screens()[0]; // specify which screen to use

	//SecondDisplay secondDisplay = new SecondDisplay(); // your widget
	ui.secondscreenDock->move((screen->geometry().x()+100), (screen->geometry().y()+100)); //the geometry shows error, must be reconsiderated
	ui.secondscreenDock->resize(600, 600); //the geometry shows error, must be reconsiderated
}